#!/bin/bash

IF=${IF:-eth0}
IPSUB=${IPSUB:-192.168.2.1}
IPSUB_1=${IPSUB_1:-192.168.1.1}
MACSUB=${MACSUB:-38:2c:4a:b0:c3:}

CTRLIF=${CTRLIF:-eth0}

VLANS="20 30 40 50 60 70 80"

__check_root()
{
	if [ $UID -ne 0 ]; then
		echo "[-] only root can do that"
		return 255
	fi
	return 0
}

vlan_create() {
	__check_root || return 255
	modprobe 8021q
	for D in $VLANS; do
		vconfig add ${IF} ${D}
	done
}

vlan_destroy() {
	__check_root || return 255
	for D in $VLANS; do
		vconfig rem ${IF}.${D}
	done	
}

ns_create() {
	__check_root || return 255
	for D in $VLANS; do
		if [ ! -e /var/run/netns/sn-${IF}-${D} ]; then
			ip netns add sn-${IF}-${D} && echo "[+] sn-${IF}-${D} created" || echo "[-] sn-${IF}-${D} could not be created"
		fi
	done
}

ns_destroy() {
	__check_root || return 255
	for D in $VLANS; do
		if [ -e /var/run/netns/sn-${IF}-${D} ]; then
			ip netns delete sn-${IF}-${D} && echo "[+] sn-${IF}-${D} destroyed" || echo "[-] sn-${IF}-${D} could not not be destroyed"
		fi
	done
}

ns_setup() {
	__check_root || return 255
	for D in $VLANS; do
		if [ ! -e /var/run/netns/sn-${IF}-${D} ]; then
			echo "[-] netspace sn-${IF}-${D} not available"
			return 1
		fi
		echo -n "[+] setting up sn-${IF}-${D}.."
		ip netns exec sn-${IF}-${D} sysctl net.ipv6.conf.all.disable_ipv6=1
		ip netns exec sn-${IF}-${D} sysctl net.ipv6.conf.default.disable_ipv6=1
		ip netns exec sn-${IF}-${D} sysctl net.ipv6.conf.lo.disable_ipv6=1

#	ip link add link ${IF}.${D} veth0 netns sn-${IF}-${D} type macvlan mode vepa &&
#	ip netns exec sn-${IF}-${D} ip link set lo up &&
#	ip netns exec sn-${IF}-${D} ip link set veth0 up &&
#	ip netns exec sn-${IF}-${D} ip addr add ${IPSUB}${D}/24 dev veth0 &&
#	echo "done" || echo "failed"
		ip link set ${IF}.${D} netns sn-${IF}-${D}
		ip netns exec sn-${IF}-${D} ip link set lo up 
		ip netns exec sn-${IF}-${D} ip link set dev ${IF}.${D} address ${MACSUB}${D}
		ip netns exec sn-${IF}-${D} ip link set ${IF}.${D} up
		echo "aaaaa"
		if [ ${D} == "80" ] || [ ${D} == "70" ] || [ ${D} == "60" ] || [ ${D} == "50" ] ; then
		    echo -n "viuuuvvv"
		    ip netns exec sn-${IF}-${D} ip addr add ${IPSUB_1}${D}/24 dev ${IF}.${D}
		else
		    echo -n "viuuuvvvaaaa"
		    ip netns exec sn-${IF}-${D} ip addr add ${IPSUB}${D}/24 dev ${IF}.${D}
		fi
		ip netns exec sn-${IF}-${D} route add -net 224.0.0.0 netmask 255.0.0.0 dev ${IF}.${D}
		ip netns exec sn-${IF}-${D} route add -net 239.0.0.0 netmask 255.0.0.0 dev ${IF}.${D}
	done
}

sshd_create() {
	__check_root || return 255
	for D in $VLANS; do
		if [ ! -e /var/run/netns/sn-${IF}-${D} ]; then
			echo "[-] netspace sn-${IF}-${D} not available"
			return 1
		fi
		echo -n "[+] launching sshd for sn-${IF}-${D}.."
		ip netns exec sn-${IF}-${D} /usr/sbin/sshd -o PidFile=/var/run/sshd-${IF}-${D}.pid &&
		echo "done" || echo "failed"
	done
}

sshd_destroy() {
	__check_root || return 255
	for D in $VLANS; do
		if [ ! -e /var/run/netns/sn-${IF}-${D} ]; then
			echo "[-] netspace sn-${IF}-${D} not available"
			return 1
		fi
		echo -n "[+] stopping sshd for sn-${IF}-${D}.."
		kill $(cat /var/run/sshd-${IF}-${D}.pid) &&
		echo "done" || echo "failed"
	done
}

iperf_create() {
	__check_root || return 255
	for D in $VLANS; do
		if [ ! -e /var/run/netns/sn-${IF}-${D} ]; then
			echo "[-] netspace sn-${IF}-${D} not available"
			return 1
		fi
		echo -n "[+] launching iperfd for sn-${IF}-${D}.."
		( ip netns exec sn-${IF}-${D} /usr/bin/iperf -s -i1 >/tmp/iperf-sn-${IF}-${D}-tcp.log & )
		( ip netns exec sn-${IF}-${D} /usr/bin/iperf -s -i1 -u>/tmp/iperf-sn-${IF}-${D}-udp.log & )
		echo "done" || echo "failed"
	done
}

iperf_destroy() {
	__check_root || return 255
	for D in $VLANS; do
		if [ ! -e /var/run/netns/sn-${IF}-${D} ]; then
			echo "[-] netspace sn-${IF}-${D} not available"
			return 1
		fi
		echo -n "[+] stopping iperfd for sn-${IF}-${D}.."
		killall -9 iperf >/dev/null 2>/dev/null
		echo "done" || echo "failed"
	done
}

nsrun() {
	local id=$1
	shift
	local pr=$@
	__check_root || return 255
	ip netns exec sn-${IF}-${id} ${pr}
}

