set background=dark

syntax enable

let g:gruvbox_contrast_dark='hard'
let g:gruvbox_italic=0
let g:gruvbox_bold=0
colorscheme gruvbox

let mapleader = ","

set nocompatible
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'flazz/vim-colorschemes'
Plugin 'tpope/vim-fugitive'
Plugin 'altercation/vim-colors-solarized'
Plugin 'vim-syntastic/syntastic'
Plugin 'morhetz/gruvbox'
Plugin 'majutsushi/tagbar.git'
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plugin 'junegunn/fzf.vim'
Plugin 'mhinz/vim-grepper'

call vundle#end()            " required
filetype plugin indent on    " required

" hybrid line numbers
:set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END
" hybrid line numbers

set ignorecase smartcase
set t_Co=256
set hlsearch
set history =1001

autocmd VimLeave * call system("xsel -ib", getreg('+'))

" select all
nnoremap <c-a> ggVG

" clipboard stuff
map <leader>y "+y
map <leader>p "+p
"vnoremap <Leader>y "*y
"nnoremap <Leader>p "*p

"buffers remember their states
set hidden

" switch between last buffer with space
nnoremap <space> <C-^>

" fzf 
nnoremap <C-p> :FZF<Cr>
nnoremap <leader>f :BLines<cr>
nnoremap <leader>F :Lines<cr>
nnoremap <leader>b :Buffers<cr>

" start scrolling at 3rd row
set scrolloff=3

" show matching brackets as they're inserted
set showmatch

nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l

imap <esc>OH <esc>0i
cmap <esc>OH <home>
nmap <esc>OH 0

nmap <esc>OF $
imap <esc>OF <esc>$a
cmap <esc>OF <end>

" Tab navigation like Firefox.
" nnoremap <C-S-tab> :tabprevious<CR>
" nnoremap <C-tab>   :tabnext<CR>
" nnoremap <C-t>     :tabnew<CR>
" inoremap <C-S-tab> <Esc>:tabprevious<CR>i
" inoremap <C-tab>   <Esc>:tabnext<CR>i
" inoremap <C-t>     <Esc>:tabnew<CR>

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_mode_map = {
    \ "mode": "passive",
    \ "active_filetypes": ["python"],
    \ "passive_filetypes": [] }
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1

" youcompleteme
let g:ycm_global_ycm_extra_conf = '~/rc_files/ycm_extra_conf.py'

" grepper Rg shortcut
command! -nargs=* -complete=file Rg GrepperRg <args>
autocmd FileType qf nnoremap <buffer> <CR> <CR>:cclose<CR>
